console.log('hello')


let trainer = {};
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {kanto: ["Brock", "Misty"], hoenn: ["May", "Max"]};

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = level;
    this.revive = function(target){
        console.log(this.name + " revive " + target.name);
        target.health += this.attack;
        console.log(target.name + "'s health is now filled to " + target.health);
    }
    this.tackle = function(target) {
        console.log(this.name + " tackled " + target.name);
        target.health -= this.attack;
        console.log(target.name + "'s health is now reduced to " + target.health);
        if (target.health <= 0) {
            target.faint();
        }
    };
    this.faint = function() {
        console.log(this.name + " fainted");
    };
}


let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("MewTwo", 100);
let Arceus = new Pokemon("Arceus", 1000);
let Shayamin = new Pokemon("Shayamin", 1000);

geodude.tackle(pikachu);
mewtwo.tackle(geodude);
pikachu.tackle(mewtwo);
mewtwo.tackle(pikachu);
Arceus.tackle(mewtwo);
Shayamin.revive(mewtwo);
Arceus.tackle(mewtwo);
Shayamin.revive(mewtwo);
Shayamin.tackle(Arceus);
Arceus.tackle(mewtwo);
Shayamin.tackle(Arceus);